/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikea2;

import com.mycompany.ikea2.repository.IkeaRepository;
import com.mycompany.ikea2.repository.entity.IkeaEntity;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author Misi
 */
@Configuration
@EnableJpaRepositories(basePackageClasses = IkeaRepository.class)
@ComponentScan(basePackages = "com.mycompany.ikea2")
@PropertySource("classpath:application.properties")
@EntityScan(basePackageClasses = IkeaEntity.class)
public class Config {
    
}
