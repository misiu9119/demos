/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikea2.service;

import com.mycompany.ikea2.dto.IkeaDTO;
import com.mycompany.ikea2.dto.IkeaFilterRequest;
import com.mycompany.ikea2.repository.IkeaRepository;
import com.mycompany.ikea2.repository.entity.IkeaEntity;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Misi
 */
@Service
public class IkeaService {

    @Autowired
    private IkeaRepository ikeaRepository;

//    @Autowired
//    public IkeaService(IkeaRepository ikeaRepository) {
//        this.ikeaRepository = ikeaRepository;
//    }

    public List<IkeaDTO> filterIkeas(/*IkeaFilterRequest request*/) {
        List<IkeaDTO> dtos = new ArrayList<>();
        List<IkeaEntity> ikeas = ikeaRepository.getIkeas();

        ikeas.forEach(item -> {
            IkeaDTO dto = new IkeaDTO();
            BeanUtils.copyProperties(item, dto);
            dtos.add(dto);
        });
        
        
return dtos;
    }
}
