/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikea2.rest;

import com.mycompany.ikea2.dto.IkeaFilterResponse;
import com.mycompany.ikea2.service.IkeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Misi
 */
@RestController
@RequestMapping("ikea_service")
public class IkeaRestController {
    
    @Autowired
    private IkeaService ikeaService;
    
    
    @RequestMapping(method = RequestMethod.GET, path = "hello")
    public String hello() {return "Hello World";}
    
    @RequestMapping(method = RequestMethod.POST, path = "filter")
    public ResponseEntity filterFares(){
        IkeaFilterResponse ikeaFilterResponse = new IkeaFilterResponse();
       ikeaFilterResponse.setIkeas(ikeaService.filterIkeas());

        return ResponseEntity.ok(ikeaFilterResponse);
    }
    
}
