/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikea2.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Misi
 */
public class IkeaFilterResponse {
    
    @JsonProperty("ikeas")
    private List<IkeaDTO> ikeas = new ArrayList<>();

    public List<IkeaDTO> getIkeas() {
        return ikeas;
    }

    public void setIkeas(List<IkeaDTO> ikeas) {
        this.ikeas = ikeas;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.ikeas);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IkeaFilterResponse other = (IkeaFilterResponse) obj;
        if (!Objects.equals(this.ikeas, other.ikeas)) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
