/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikea2.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Misi
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IkeaDTO implements Serializable {
    
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Amount")
    private int amount;
}
