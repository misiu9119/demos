/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikea2.repository.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Misi
 */
@Entity
@Table(name = "ikea")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IkeaEntity implements Serializable {
    
    @Id
    @Column(name = "Name", length = 10)
    private String name;
    @Column(name = "Amount")
    private int amount;
    
    
    
    
}
