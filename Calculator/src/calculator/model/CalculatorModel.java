/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.model;



/**
 *
 * @author Misi
 */
public class CalculatorModel {

    private long numToDisplay;
    private long numInMemory;
    private Operation operation;
    private static String ADD_SIGN;
  

    public CalculatorModel() {
        numToDisplay = 0;
        numInMemory = 0;
        
    }

    public static String getADD_SIGN() {
        return ADD_SIGN;
    }
    
    

    public long getNumToDisplay() {
        return numToDisplay;
    }

    public void setNumToDisplay(long numToDisplay) {
        this.numToDisplay = numToDisplay;
    }

    public long getNumInMemory() {
        return numInMemory;
    }

    public void setNumInMemory(long numInMemory) {
        this.numInMemory = numInMemory;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    
   
    }




