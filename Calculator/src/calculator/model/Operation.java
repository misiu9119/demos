/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.model;

/**
 *
 * @author Misi
 */
public enum Operation {
    ADD("+"), SUB("-");
    
    String sign;

    private Operation(String sign) {
        this.sign = sign;
        
        
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
    
    
    
}
