/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.view;

import calculator.controller.CalculatorController;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author Misi
 */
public class CalculatorView extends JFrame {

    private JPanel panel;
    private JPanel logPanel;
    private GridBagLayout gbl;
    private GridBagConstraints gbc = new GridBagConstraints();
   // private static int num;

    private JTextField textField;
    private JButton btNum;
    private JButton btPlus;
    private JButton bt0;

    private JButton btCancel;
    private JButton btEq;
    

    private CalculatorController cc;
    
    

    public CalculatorView(CalculatorController cc) throws HeadlessException{

        this.cc = cc;
        GridBagLayout gbl = new GridBagLayout();
        panel = new JPanel(gbl);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setTitle("Calculator");
        setSize(400, 500);
        textField();
        numericButtons();
        zeroButton();
        eqButton();
        cancelButton();
        plusButton();
     //   readLog();

    }
    
    public void readLog(){
    
        JList logList = new JList();
        JButton btLogRefresh = new JButton("Refresh");
        btLogRefresh.addActionListener(e -> {logList.setListData(cc.getLogs().toArray());});
        
        gbc.gridx = 0;
        gbc.gridy = 5;
        
        logPanel.add(new JScrollPane(logList));
        logPanel.add(new JPanel().add(btLogRefresh), gbc);
    }

    public void textField() {
        textField = new JTextField("0");
        textField.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        textField.setFont(new Font("Times New Roman", Font.PLAIN, 40));
        textField.setEditable(false);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 5;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(textField, gbc);
    }

    public void cancelButton() {
        btCancel = new JButton("C");
        gbc.gridx = 3;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(btCancel, gbc);

        btCancel.addActionListener((e) -> {
            cc.handleCancelButton("C");
        });// {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//              cc.handleCancelButton();
//            }
//        });
        setVisible(true);
    }

    public void plusButton() {
        btPlus = new JButton("+");
        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.BOTH;
        btPlus.addActionListener((e) -> {
            cc.handlePlusButton("+");
        });

        panel.add(btPlus, gbc);
    }

    public void eqButton() {
        btEq = new JButton("=");
        gbc.gridx = 3;
        gbc.gridy = 3;
        gbc.gridheight = 2;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.BOTH;
        btEq.addActionListener((e) -> {
            cc.handleEqualsButtonClick();
        });
        panel.add(btEq, gbc);
    }

    public void zeroButton() {
        bt0 = new JButton(String.valueOf(0));
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridheight = 1;
        gbc.gridwidth = 3;
        gbc.fill = GridBagConstraints.BOTH;
        bt0.addActionListener((e) -> {
            cc.handleNumericButtons("0");
        });

        panel.add(bt0, gbc);
        add(panel);

    }



    public void setTextfieldValue(long value) {
        textField.setText(Long.toString(value));
    }

    public long getTextfieldValue() {
        long value = Long.parseLong(textField.getText());
        return value;
    }

    public void numericButtons() {
        int num = 0;
        for (int i = 3; i >= 0; i--) {
            
            for (int j = 0; j < 3; j++) {
                num++;
                gbc.gridx = j;
                gbc.gridy = i;
                gbc.gridheight = 1;
                gbc.weightx = 1;
                gbc.weighty = 1;
                gbc.gridwidth = 1;
                gbc.fill = GridBagConstraints.BOTH;

                btNum = new JButton(String.valueOf(num));
                panel.add(btNum, gbc);
                final int numFinal = num;
                btNum.addActionListener((e) -> {
                    cc.handleNumericButtons(String.valueOf(numFinal));
                });
                
            }
        }
    }
}
