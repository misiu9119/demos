/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator.controller;

import calculator.view.CalculatorView;
import calculator.model.CalculatorModel;
import calculator.model.Operation;
import calculator.service.CalculatorService;
import java.util.List;
import logger.Logger;

/**
 *
 * @author Misi
 */
public class CalculatorController {

    private CalculatorModel model;
    private CalculatorView view;
    private CalculatorService service;
    private Logger logger;
   
    
    private final Long DEFAULT_NUM = 0L;
    private StringBuffer dispText = new StringBuffer("");
    private StringBuffer logText = new StringBuffer("");
    

    public CalculatorController() {
        this.model = new CalculatorModel();
        this.view = new CalculatorView(this);
        this.service = new CalculatorService();
        this.logger = Logger.getInstance();
    }

    public Long getDEFAULT_NUM() {
        return DEFAULT_NUM;
    }

    public StringBuffer getLogText() {
        return logText;
    }

    private void updateViewFromModel(long num) {
        view.setTextfieldValue(num);
    }

    public void handleEqualsButtonClick() {
        System.out.println("Equals Button Pushed");
        switch(model.getOperation()){
            case ADD:
                model.setNumToDisplay(service.addNumbers(model.getNumInMemory(), model.getNumToDisplay()));
                break;
                
        }
       
        model.setNumInMemory(model.getNumToDisplay());
        System.out.println("Display: " + model.getNumToDisplay());
        System.out.println("Memory: " + model.getNumInMemory());
        updateViewFromModel(model.getNumInMemory());
        logText.append(" = ").append(model.getNumToDisplay());
        logger.log(logText.toString());
    }

    public void handleNumericButtons(String btText) {
        if (btText.equals(DEFAULT_NUM) && view.getTextfieldValue() == DEFAULT_NUM) {

        } else {
            String stringNumber = Long.toString(view.getTextfieldValue()).concat(btText);
            Long longNum = Long.parseLong(stringNumber);
            updateViewFromModel(longNum);
            model.setNumToDisplay(longNum);
           
            logText.append(model.getNumToDisplay());
        }

        System.out.println(btText + " Button Pushed");
        System.out.println("Display: " + model.getNumToDisplay());
        System.out.println("Memory: " + model.getNumInMemory());
    }

    public void handlePlusButton(String btText) {
        updateViewFromModel(0);
        // 5 + 4 + 2 + 4 = 
        model.setNumInMemory(service.addNumbers(model.getNumInMemory(), model.getNumToDisplay()));
        model.setOperation(Operation.ADD);
        logText.append(dispText.toString()).append(" ").append(Operation.ADD.getSign());
        System.out.println("+ Button Pushed");
        service.addNumbers(model.getNumInMemory(), model.getNumToDisplay());
        System.out.println("Display: " + model.getNumToDisplay());
        System.out.println("Memory: " + model.getNumInMemory());
   //     logger.writeInFile(stringToFile);

    }

    public void handleCancelButton(String btText) {
        updateViewFromModel(0);
        System.out.println("Cancel Button Pushed");
        model.setNumToDisplay(0);
        model.setNumInMemory(0);
        System.out.println("Display: " + model.getNumToDisplay());
        System.out.println("Memory: " + model.getNumInMemory());
    }

    public List<String> getLogs(){
        return logger.read();
    }
}

