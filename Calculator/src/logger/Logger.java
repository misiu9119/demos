/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import calculator.controller.CalculatorController;
import calculator.view.CalculatorView;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Misi
 */
public class Logger {

    private static class LogHolder {

        private static final Logger INSTANCE = new Logger();
    }

    public static Logger getInstance() {
        return LogHolder.INSTANCE;
    }

    public void log(String logText) {

        try (PrintWriter pw = new PrintWriter(new FileWriter("log.txt", true))) {
            pw.println(logText);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> read() {
        int i = 0;
        List<String> list = new ArrayList();
        try (BufferedReader br = new BufferedReader(new FileReader("log.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

}
