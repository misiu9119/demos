/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.web;

import com.mycompany.todohibernate.impl.TodoDao;
import com.mycompany.todohibernate.model.Todo;
import com.mycompany.todohibernate.model.User;
import com.mycompany.todohibernate.repository.UserRepository;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Misi
 */
@WebServlet(name = "TodoServlet", urlPatterns = {"/TodoServlet"})
public class TodoServlet extends HttpServlet {

    @Inject
    TodoDao todoDao;

    @EJB
    UserRepository userRepository;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        req.setAttribute("todoList", todoDao.getAll());
        req.setAttribute("userList", userRepository.getUsers());
        req.getRequestDispatcher("/todo.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        Todo todo = new Todo();

        todo.setDescription(req.getParameter("desc"));
        todo.setSummary(req.getParameter("sum"));

        List<User> usersWithSpecifiedUserName = userRepository.getUserByUsername(req.getParameter("username"));

        if (usersWithSpecifiedUserName.isEmpty()) {
            User user = new User();
            user.setName(req.getParameter("username"));
            userRepository.addUser(user);
            todo.setUser(user);
        } else {
            todo.setUser(usersWithSpecifiedUserName.get(0));
        }

        todoDao.save(todo);

        resp.sendRedirect(this.getServletName());
    }

}