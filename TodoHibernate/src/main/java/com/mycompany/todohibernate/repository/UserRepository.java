/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.repository;

/**
 *
 * @author Misi
 */

import com.mycompany.todohibernate.model.User;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;


@Stateless
@LocalBean
public class UserRepository {
    @PersistenceContext(unitName = "TodoHibernatePU")
    EntityManager em;

    public List<User> getUsers() {
        Query q = em.createQuery("select g from User g", User.class);
        return q.getResultList();
    }

    @Transactional
    public void addUser(User user) {
        em.persist(user);
    }

    public List<User> getUserByUsername(String username) {
        return em.createQuery("SELECT c FROM User c WHERE c.name = :name")
                .setParameter("name", username)
                .getResultList();
    }
}