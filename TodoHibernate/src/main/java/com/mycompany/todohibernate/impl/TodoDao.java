/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.impl;

import com.mycompany.todohibernate.dao.Dao;
import com.mycompany.todohibernate.model.Todo;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Misi
 */
@Stateless
@LocalBean
public class TodoDao implements Dao<Todo> {

    @PersistenceContext(name = "TodoHibernatePU")
    private EntityManager entityManager;

    @Override
    public List<Todo> getAll() {
        Query query = entityManager.createQuery("SELECT t FROM Todo t", Todo.class);
        return query.getResultList();
    }

    @Override
    public void save(Todo t) {
        entityManager.persist(t);
    }

    @Override
    public void delete(Todo t) {
        entityManager.remove(t);
    }

    @Override
    public Optional<Todo> get(Long id) {
        return Optional.ofNullable(entityManager.find(Todo.class, id));
    }

    @Override
    public void update(Todo t, String[] params) {
        Todo todoToUpdate = getAll().get(t.getId().intValue());

        todoToUpdate.setSummary(params[0]);
        todoToUpdate.setDescription(params[1]);

        entityManager.merge(t);
    }

    public List<Todo> getTodosByUser(long queryId) {
        return entityManager.createQuery("select c FROM Todo c WHERE c.id=queryId")
            .setParameter("id", queryId)
            .getResultList();
    }

    public List<Todo> getFilteredTodos(Todo todo) {
        return entityManager.createQuery("Select t FROM Todo t WHERE t.summary LIKE :summary AND t.description LIKE :description AND t.user.name LIKE :username")
            .setParameter("description", "%" + todo.getDescription() + "%")
            .setParameter("summary", "%" + todo.getSummary() + "%")
            .setParameter("username", "%" + todo.getUser().getName() + "%")
            .getResultList();
    }

}
