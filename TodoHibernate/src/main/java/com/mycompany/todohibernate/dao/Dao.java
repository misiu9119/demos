/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.dao;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author Misi
 */
public interface Dao <T> {
    List<T> getAll();

    void save(T t);

    void delete(T t);

    Optional<T> get(Long id);

    void update(T t, String[] params);
}
