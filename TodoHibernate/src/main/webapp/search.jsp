<%-- 
    Document   : search
    Created on : 2019.06.15., 17:01:21
    Author     : Misi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style><%@include file="WEB-INF/style.css" %></style>
        <title>Todo Page</title>
    </head>
    <body style="margin: 0 auto; width: 600px">
        <ul class="nav nav-fill">
            <li class="nav-item">
              <a class="nav-link active" href="TodoServlet">Todo</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="UserServlet">User</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Search</a>
            </li>
        </ul>
        <h1>Search form</h1>
        <form method="POST">
            <div class="form-group">
                <input type="text" placeholder="Summary..." name="sum" class="form-control" />
            </div>
            <div class="form-group">
                <input type="text" placeholder="Description..." name="desc" class="form-control" />
            </div>
            <div class="form-group">
                <input type="text" placeholder="Username..." name="username" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Search Todo</button>
        </form>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Desc</th>
                    <th>Sum</th>
                    <th>User</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${todoList}" var="todo">
                <tr>
                    <td>${todo.id}</td>
                    <td>${todo.description}</td>
                    <td>${todo.summary}</td>
                    <td>${todo.getUser().getName()}</td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>