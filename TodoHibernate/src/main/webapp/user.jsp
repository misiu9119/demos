<%-- 
    Document   : user
    Created on : 2019.06.15., 17:02:48
    Author     : Misi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Users Page</title>
        <style><%@include file="WEB-INF/style.css" %></style>
    </head>
    <body>
        <ul class="nav nav-fill">
            <li class="nav-item">
              <a class="nav-link active" href="TodoServlet">Todo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">User</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="SearchServlet">Search</a>
            </li>
        </ul>
        <h1>User form</h1>
        <form method="POST">
            <div class="form-group">
                <input type="text" placeholder="Name..." name="username" class="form-control" />
            </div>
            <button type="submit" class="btn btn-primary">Add User</button>
        </form>
        <br>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${userList}" var="user">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.name}</td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>