/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.repository;

import avengersgyak.heroes.Hero;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Misi
 */
public class ShipRepository {

    private final List<Ship> ships = new ArrayList<>();

    public ShipRepository() {
        ships.add(new Ship());
    }

    public List<Ship> getShips() {
        return ships;
    }

    private Ship getLastShip() {
        return ships.get(ships.size() - 1);
    }

    private void addShipWithEmptySeatIfItFull() {
        if (getLastShip().size() >= Ship.SHIP_SEAT_LIMIT) {
            ships.add(new Ship());
        }
    }

    public void add(Hero hero) {
        addShipWithEmptySeatIfItFull();
        Ship lastShip = getLastShip();
        lastShip.addHero(hero);
    }

    public Set<Hero> getAllHeroes(){
        return ships.stream().map(Ship::getHeroes)
                .collect(Collectors.toSet())
                .stream()
                .flatMap(f -> f.stream())
                .collect(Collectors.toSet());
    }
}
