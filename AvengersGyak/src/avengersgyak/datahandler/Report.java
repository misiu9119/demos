/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.datahandler;

import avengersgyak.comparators.HeroNameComparator;
import avengersgyak.heroes.BornOnEarthHero;
import avengersgyak.heroes.Hero;
import avengersgyak.heroes.stones.StoneType;
import avengersgyak.repository.Ship;
import avengersgyak.repository.ShipRepository;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 *
 * @author Misi
 */
public class Report {

    private static final String FILE_NAME = "Avengers.txt";

    private ShipRepository shipRepository;

    public Report(ShipRepository shipRepository) {
        this.shipRepository = shipRepository;
    }
    
    public void generateReport() throws IOException{
    reportIntoFile();
    reportNumberOfBornOnEarth();
    reportPassportNubers();
    reportStrongestHero();
    reportNumberOfStonesByShips();
    reportNumberOfShips();
    }

    public void reportIntoFile() throws IOException {
        try (PrintWriter pw = new PrintWriter(new FileWriter(FILE_NAME))) {
            Set<Hero> heroes = new TreeSet<>(new HeroNameComparator());
            heroes.addAll(shipRepository.getAllHeroes());

            heroes.forEach(pw::println);
        }
    }

    private Map<StoneType, Integer> groupBystones(Ship ship) {
        return ship.getHeroes().stream()
                .collect(Collectors.toMap(
                        (Hero hero) -> hero.getStoneType(),
                        (Hero hero) -> 1,
                        (value1, value2) -> value1 + value2,
                        HashMap::new
                )
                );
    }

    public void reportNumberOfStonesByShips() {
        System.out.println("Number of stones by ships: ");

        shipRepository.getShips().forEach((Ship ship) -> {
            System.out.println(ship.toString());
            groupBystones(ship)
                    .entrySet()
                    .forEach(entry -> System.out.println(entry.getKey() + " " + entry.getValue())
                    );
        });
    }

    public void reportNumberOfBornOnEarth() {
        long heroes = shipRepository.getAllHeroes().stream()
                .filter(f -> f instanceof BornOnEarthHero).count();

        System.out.println("Number of heroes born on the Earth: " + heroes);
    }

    public void reportStrongestHero() {
        Optional<Hero> hero = shipRepository.getAllHeroes().stream()
                .max(Comparator.comparingInt(Hero::getPower));

        if (hero.isPresent()) {
            System.out.println("The strongest hero: " + hero.get());
        }
    }

    public void reportPassportNubers() {
        System.out.println("Passport numbers: ");
        shipRepository.getAllHeroes().stream()
                .filter(f -> f instanceof BornOnEarthHero)
                .map(hero -> ((BornOnEarthHero) hero).getPassport())
                .forEach(passport -> System.out.println("Passport: " + passport.getNumber()));

    }

public void reportNumberOfShips(){
    System.out.println("Number of ships: " + shipRepository.getShips().size());

}
}

