/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.datahandler;

import avengersgyak.exception.HeroException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Misi
 */
public class ConsoleReader {
    
    private static final String STOP_COMMAND = "exit";
    
    public static void readConsole() throws IOException, HeroException{
        CommandParser parser = new CommandParser();
        
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            String line;
            
            while((line = br.readLine()) != null && !STOP_COMMAND.equalsIgnoreCase(line)) {
                parser.parse(line);
            }
            parser.report();
        }
    }
    
}
