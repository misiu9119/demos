/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.datahandler;

import avengersgyak.exception.HeroException;
import avengersgyak.factory.HeroFactory;
import avengersgyak.heroes.Hero;
import avengersgyak.repository.ShipRepository;
import java.io.IOException;

/**
 *
 * @author Misi
 */
public class CommandParser {
    
    private static final String DELIMITER = ";";
    
    private static final int NUMBER_OF_ARGS = 4;
    
    private ShipRepository shiprepository;
    
    public void parse(String command) throws HeroException{
        
        String commands[] = command.split(DELIMITER);
        
        if (commands.length != NUMBER_OF_ARGS){
            throw new HeroException("Args number less than 4");
            
        }
        Hero hero = HeroFactory.create(commands[0], commands[1], commands[2], commands[3]);
            shiprepository.add(hero);
                
        }
        
    public void report() throws IOException{
        new Report(shiprepository).generateReport();
    
    }
        
    }

