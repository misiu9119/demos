/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.factory;

import avengersgyak.exception.HeroException;
import avengersgyak.heroes.AlienHero;
import avengersgyak.heroes.BornOnEarthHero;
import avengersgyak.heroes.Hero;
import avengersgyak.heroes.stones.StoneType;

/**
 *
 * @author Misi
 */
public class HeroFactory {
    
    public static Hero create(String name, String power, String stoneType, String bornInEarth) throws HeroException{
        double heroPower = Double.parseDouble(power);
        StoneType stone =  StoneType.valueOf(stoneType);
        
        switch (bornInEarth){
            case "0": return new AlienHero(name, heroPower, stone);
            case "1": return new BornOnEarthHero(name, heroPower, stone);
        }
      throw new HeroException("4th Arg just 0 or 1");
    } 
    
}
