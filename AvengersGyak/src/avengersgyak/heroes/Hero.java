/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.heroes;

import avengersgyak.heroes.stones.StoneType;
import java.util.Objects;

/**
 *
 * @author Misi
 */
public abstract class Hero {
    
    private String name;
    private double power;
    private StoneType stoneType;

    public Hero(String name, double power, StoneType stoneType) {
        this.name = name;
        this.power = power;
        this.stoneType = stoneType;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public StoneType getStoneType() {
        return stoneType;
    }

    public void setStoneType(StoneType stoneType) {
        this.stoneType = stoneType;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.power) ^ (Double.doubleToLongBits(this.power) >>> 32));
        hash = 97 * hash + Objects.hashCode(this.stoneType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Hero other = (Hero) obj;
        if (Double.doubleToLongBits(this.power) != Double.doubleToLongBits(other.power)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.stoneType != other.stoneType) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Hero{" + "name=" + name + ", power=" + power + ", stoneType=" + stoneType + '}';
    }
    
    
    
    
}
