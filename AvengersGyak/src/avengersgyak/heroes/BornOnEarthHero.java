/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.heroes;

import avengersgyak.heroes.abilities.Fly;
import avengersgyak.heroes.passport.Passport;
import avengersgyak.heroes.stones.StoneType;
import java.util.Objects;

/**
 *
 * @author Misi
 */
public class BornOnEarthHero extends Hero implements Fly{

    private final Passport passport;

    public BornOnEarthHero(String name, double power, StoneType stoneType) {
        super(name, power, stoneType);
        passport = new Passport();
    }

    public Passport getPassport() {
        return passport;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.passport);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BornOnEarthHero other = (BornOnEarthHero) obj;
        if (!Objects.equals(this.passport, other.passport)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BornOnEarthHero{" + "passport=" + passport + '}';
    }
    
    
    
    
    @Override
    public void fly() {
        System.out.println("I can fly");    
    }
    
}
