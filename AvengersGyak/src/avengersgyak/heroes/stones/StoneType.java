/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.heroes.stones;

/**
 *
 * @author Misi
 */
public enum StoneType {
    SPACE("blue", 4),
    MIND("yellow", 6),
    TIME("green", 3), 
    POWER("purple", 7),
    SOUL("orange", 5),
    REALITY("red", 4);
    
    private final String color;
    private final int stonePower;

    private StoneType(String color, int stonePower) {
        this.color = color;
        this.stonePower = stonePower;
    }

    public String getColor() {
        return color;
    }

    public int getStonePower() {
        return stonePower;
    }
    
    
    
}
