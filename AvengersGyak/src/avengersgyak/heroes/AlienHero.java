/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.heroes;

import avengersgyak.heroes.abilities.Flame;
import avengersgyak.heroes.abilities.Swim;
import avengersgyak.heroes.stones.StoneType;

/**
 *
 * @author Misi
 */
public class AlienHero extends Hero implements Swim, Flame {

     public AlienHero(String name, double power, StoneType stoneType) {
        super(name, power, stoneType);
    }

    @Override
    public String toString() {
        return "AlienHero{" + '}';
    }

   
    
    

    @Override
    public void swim() {
        System.out.println("I can swim");    }

    @Override
    public void Flame() {
        System.out.println("I have a flamethrower");
    }
    
    
    
}
