/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.comparators;

import avengersgyak.heroes.Hero;
import java.util.Comparator;

/**
 *
 * @author Misi
 */
public class HeroNameComparator implements Comparator<Hero>{

    @Override
    public int compare(Hero o1, Hero o2) {
        return o1.getName().compareTo(o2.getName());
    }
    
}
