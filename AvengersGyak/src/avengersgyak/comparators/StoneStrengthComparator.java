/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avengersgyak.comparators;

import avengersgyak.heroes.Hero;
import avengersgyak.heroes.stones.StoneType;
import java.util.Comparator;

/**
 *
 * @author Misi
 */
public class StoneStrengthComparator implements Comparator<Hero> {

    @Override
    public int compare(Hero o1, Hero o2) {
        return o1.getStoneType().getStonePower() - o2.getStoneType().getStonePower();
    }

}
