
package instrumentshop.Application;

import instrumentshop.products.PercussionInstrument;
import instrumentshop.repository.ProductRepo;
import java.util.ArrayList;
import java.util.List;


public class Application {
    public static void main(String[] args) {
        PercussionInstrument product = new PercussionInstrument(10000);
        
        List<PercussionInstrument> list = new ArrayList<>();
        list.add(new PercussionInstrument (5000));
        list.add(new PercussionInstrument(2000));
        
        System.out.println(list.get(0).compareTo(list.get(1)));
        ProductView.listAll();
        ProductRepo.removeProduct(1);
        ProductView.listAll();
    }
}
