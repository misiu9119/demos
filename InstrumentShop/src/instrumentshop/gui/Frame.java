
package instrumentshop.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class Frame extends JFrame implements ActionListener {
    private JButton btLogin = new JButton("Login");
    private JButton btSend = new JButton("Send");
    private JTextField tfLoginName = new JTextField(10);
    private JPasswordField jpfPassword = new JPasswordField(10);
    private JTextField tfMessage = new JTextField(10);
    private JTextArea textArea = new JTextArea();

    public Frame() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(500, 600);
    setLocationRelativeTo(this);
    JPanel pnLogin = new JPanel(new GridLayout(1, 2));
    pnLogin.setBackground(Color.RED);
    JPanel pnLoginTf = new JPanel();
    pnLoginTf.add(new JLabel("name: "));
    
    
    pnLoginTf.add(tfLoginName);
    JPanel pnLoginPf = new JPanel();
    pnLoginPf.add(new JLabel("password: "));
    pnLoginPf.add(jpfPassword);
    pnLogin.add(btLogin);
    
    JPanel pnLoginData = new JPanel(new GridLayout(2, 1));
    pnLoginData.add(pnLoginTf);
    pnLoginData.add(pnLoginPf);
    pnLogin.add(pnLoginData);
    JPanel pnLoginBt = new JPanel();
    pnLoginBt.add(btLogin);
    pnLogin.add(pnLoginBt);
    
    JPanel pnSend = new JPanel();
    pnSend.add(new JLabel("message: "));
    pnSend.add(tfMessage);
    pnSend.add(btSend);
    JPanel pn = new JPanel(new GridLayout(2, 1));
    pn.add(pnLogin);
    pn.add(pnSend);
    add(textArea);
    add(pn, BorderLayout.PAGE_START);
    btLogin.addActionListener(this);
    btSend.addActionListener(this);
    btSend.setEnabled(false); //amíg nincs login, nem küldhedsz üzenetet
    setVisible(true);
    }
     public static void main(String[] args) {
        new Frame();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btLogin) {
            //login meghívása
            System.out.println(tfLoginName.getText());
            System.out.println(String.valueOf(jpfPassword.getPassword()));
            btSend.setEnabled(true);
            btLogin.setText("Logout"); // a modellről kell validálnom a logint
        }
        if (e.getSource() == btSend) {
                textArea.setText(textArea.getText() + tfMessage.getText() + "\n");
        }
    }
    
}


