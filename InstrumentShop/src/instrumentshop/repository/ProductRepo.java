
package instrumentshop.repository;

import instrumentshop.products.Product;
import java.util.ArrayList;
import java.util.List;


public class ProductRepo {
    private static List<Product> list = new ArrayList<>();
        
    public static void addProduct(Product a) {
        list.add(a);
    }
    
    public static int getSize() {
        return list.size ();
    }
    
    public static Product get(int b) {
        return list.get(b);
    }
    
    public static Product getProduct(int b) {
        return list.get(b);
    }
    
    public static void removeProduct(int a) {
        list.remove(a);
    }
    
    }


