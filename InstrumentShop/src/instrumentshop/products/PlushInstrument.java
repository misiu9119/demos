

package instrumentshop.products;

import instrumentshop.products.Product;

enum TypePlush{
    harp,
    guitar,
    lute;
    
}

public class PlushInstrument extends Product{
    
    public PlushInstrument(int price) {
        super(price);
    }

    @Override
    public String toString() {
        return "PlushInstrument{" +  super.toString() +'}';
    }
    
    
}
