package instrumentshop.products;

import instrumentshop.products.Product;

enum TypeKeyboard {
    piano,
    harmonica,
    organ;
}

public class KeyboardInstrument extends Product {

    public KeyboardInstrument(int price) {
        super(price);
    }

    @Override
    public String toString() {
        return "KeyboardInstrument{" + super.toString() + '}';
    }

}
