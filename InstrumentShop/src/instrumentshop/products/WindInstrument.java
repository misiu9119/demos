
package instrumentshop.products;

import instrumentshop.products.Product;

enum TypeWind{
    Oboe,
    Bassoon,
    Flute;
}
public class WindInstrument extends Product{
    
    public WindInstrument(int price) {
        super(price);
    }

    @Override
    public String toString() {
        return "WindInstrument{" + super.toString() +'}';
        
    }
    
    
}
