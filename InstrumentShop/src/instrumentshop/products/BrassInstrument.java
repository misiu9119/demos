
package instrumentshop.products;

import instrumentshop.products.Product;

enum TypeBrass{
    Trumpet,
    Trombone;
}
public class BrassInstrument extends Product{
    
    public BrassInstrument(int price) {
        super(price);
    }

    @Override
    public String toString() {
        return "BrassInstrument{" + super.toString() +'}';
    }
    
}
