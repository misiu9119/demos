
package instrumentshop.products;
import static java.lang.Integer.compare;


public class Product implements Comparable<Product> {


   protected int price;
    
   
   public int compareTo(Product otherPrice) {
        int thisPrice = this.price;
        int otherPr = otherPrice.price;
        return (thisPrice < otherPr ? -1 : (thisPrice == otherPr ? 0 : 1));
    }

    @Override
    public String toString() {
        return "Product{" + "price=" + price + '}';
    }

    public Product(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
}

