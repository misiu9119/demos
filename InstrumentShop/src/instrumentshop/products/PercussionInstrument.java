
package instrumentshop.products;

import instrumentshop.products.Product;


enum TypePercussion{
    cajon,
    bongo,
    drum,
    shaker,
    cymbal;
}

public class PercussionInstrument extends Product{
    
    public PercussionInstrument(int price) {
        super(price);
    }

    @Override
    public String toString() {
        return "PercussionInstrument{" + super.toString() +'}';
    }
    
}
